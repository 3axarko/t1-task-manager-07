package ru.t1.zkovalenko.tm;

import ru.t1.zkovalenko.tm.constant.ArgumentsConst;
import ru.t1.zkovalenko.tm.constant.TerminalConst;
import ru.t1.zkovalenko.tm.model.Command;
import ru.t1.zkovalenko.tm.utils.FormatUtil;

import java.util.Locale;
import java.util.Scanner;

public final class Application {

    public static void main(String[] args) {
        if (processArguments(args)) exit();

        System.out.println("** Welcome to Task-Manager **");

        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter Command:");
            final String command = scanner.nextLine();
            System.out.println("---");
            processArgument(command);
        }
    }

    private static void processArgument(final String arg) {
        switch (arg.toLowerCase(Locale.ENGLISH)) {
            case TerminalConst.VERSION:
            case ArgumentsConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
            case ArgumentsConst.HELP:
                showHelp();
                break;
            case TerminalConst.ABOUT:
            case ArgumentsConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.INFO:
            case ArgumentsConst.INFO:
                showInfo();
                break;
            case TerminalConst.EXIT:
                exit();
            default:
                showError();
        }
    }

    private static void exit() {
        System.exit(0);
    }

    private static boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public static void showError() {
        System.err.println("Command Not Found");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.7.1");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Who is the best: Read below");
        System.out.println("Developer: 3axarko");
        System.out.println("And he's really good");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println("\nType key or command name\n");
        System.out.println(Command.VERSION);
        System.out.println(Command.ABOUT);
        System.out.println(Command.HELP);
        System.out.println(Command.INFO);
        System.out.println(Command.EXIT);
    }

    public static void showInfo() {
        System.out.println("Available processors (cores): " + Runtime.getRuntime().availableProcessors());

        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + FormatUtil.convertBytes(freeMemory));

        final long maxMemory = Runtime.getRuntime().maxMemory();
        String maxMemoryChecked = maxMemory == Long.MAX_VALUE ? "no limit" : FormatUtil.convertBytes(maxMemory);
        System.out.println("Maximum memory: " + maxMemoryChecked);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + FormatUtil.convertBytes(totalMemory));

        System.out.println("Usage memory: " + FormatUtil.convertBytes(totalMemory - freeMemory));
    }

}
