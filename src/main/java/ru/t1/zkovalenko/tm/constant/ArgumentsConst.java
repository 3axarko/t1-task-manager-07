package ru.t1.zkovalenko.tm.constant;

public final class ArgumentsConst {

    public final static String VERSION = "-v";

    public final static String ABOUT = "-a";

    public final static String HELP = "-h";

    public final static String INFO = "-i";

}
