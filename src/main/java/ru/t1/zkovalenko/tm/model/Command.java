package ru.t1.zkovalenko.tm.model;

import ru.t1.zkovalenko.tm.constant.ArgumentsConst;
import ru.t1.zkovalenko.tm.constant.TerminalConst;

public class Command {

    public static Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentsConst.VERSION,
            "Show app version"
    );

    public static Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentsConst.ABOUT,
            "Who did it? And why?"
    );

    public static Command HELP = new Command(
            TerminalConst.HELP, ArgumentsConst.HELP,
            "App commands"
    );

    public static Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Quit from program"
    );

    public static Command INFO = new Command(
            TerminalConst.INFO, ArgumentsConst.INFO,
            "Info about system"
    );

    private String name = "";
    private String argument;
    private String description = "";

    public Command() {
    }

    public Command(String name, String argument, String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString(){
        String result = "";
        if (argument != null && !argument.isEmpty()) result += argument;
        result += "\t";
        if (name != null && !name.isEmpty()) result += name + "\t\t";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
